﻿using UnityEngine;
using UnityEditor;
using System.IO;
using UnityEditor.Callbacks;
using System.Collections.Generic;
using UnityEditor.iOS.Xcode;

public class PostProcessTestFairy : MonoBehaviour {

    private static string TestFairyAppId = "<Insert your Test Fairy App Id here!!!!!>";

	[PostProcessBuild]
	public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject) {
	
		if (target == BuildTarget.iPhone) {
			string filename = pathToBuiltProject + "/Classes/UnityAppController.mm"; 
			string[] allLines = File.ReadAllLines(filename);
			List<string> allNewLines = new List<string>();
			allNewLines.Add("#import \"TestFairy.h\"");

			for (int i = 0; i < allLines.Length; i++) {
				if (allLines[i].Contains("-> applicationDidFinishLaunching()")) {
					allNewLines.Add("\t[TestFairy begin:@\"" + TestFairyAppId + "\"];");
				}
				allNewLines.Add(allLines[i]);
			}

			File.WriteAllLines( filename, allNewLines.ToArray());
		}

		UnityEditor.iOS.Xcode.PBXProject project = new UnityEditor.iOS.Xcode.PBXProject();
		string projectPath = pathToBuiltProject + "/Unity-iPhone.xcodeproj/project.pbxproj";
		project.ReadFromFile(projectPath);
		string targetGuid = project.TargetGuidByName("Unity-iPhone");
		project.AddFrameworkToProject(targetGuid, "CoreMedia.framework", false);
		project.AddFrameworkToProject(targetGuid, "CoreMotion.framework", false);
		project.AddFrameworkToProject(targetGuid, "AVFoundation.framework", false);
		project.AddFrameworkToProject(targetGuid, "SystemConfiguration.framework", false);
		project.AddFrameworkToProject(targetGuid, "OpenGLES.framework", false);
		project.WriteToFile(projectPath);
	}

}
