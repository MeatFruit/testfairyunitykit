# README #

### What is this repository for? ###

The TestFairy iOS lib will automatically be included in your Unity game when you build it in iOS mode.

### How do I get set up? ###

* Add "PostProcessTestFairy.cs" to the folder "Assets/Editor/"
* Add all the files from the unity3d project https://bitbucket.org/Unity-Technologies/xcodeapi to the folder "Assets/Editor/XCodeApi"
* Add "TestFairy.h" and "libTestFairy.a" (Download the iOS TestFairy lib from http://TestFairy.com) to the folder "Assets/Plugins/iOS"
* Update the string "TestFairyAppId" within file "PostProcessTestFairy.cs" with the app id which has been provided by TestFairy


### External Code/Libs ###

The script is tested on

* Unity3d v4.5.4, v4.6.0, v5.0.2, v5.1.0 (https://unity3d.com)
* TestFairy iOS lib v0.17, v0.20, v1.0, v1.3 and v1.4.4 (http://TestFairy.com)
* XCodeApi snapshort from 15.11.2014 (https://bitbucket.org/Unity-Technologies/xcodeapi)

### Contact ###

You can contact us via the twitter handle @meat_and_fruit